from DictDiffer import DictDiffer
import time
class Car2go:
	# Init function
	def __init__(self, idle = {}, startRental= {}, endRental={}):
		#print 'Hello'
		self.idle = idle
		self.startRental = startRental
		self.endRental = endRental
	
	# override to print the class variables
	def __str__(self):
	        return "From str method of this class: idle is %s, startRental is %s, endRental is %s" % (self.idle, self.startRental, self.endRental)
	
	# Setter for Idle
	def setIdle(self, idle):
		#idle_var = availability['idle']
		self.idle.update(idle)

	# Setter for Smart Rental
	def setStartRental(self, startRental):
		#startRental_var = availability['startRental']
		self.startRental.update(startRental)

	# Setter for End Rental
	def setEndRental(self, endRental):
		#endRental_var = availability['endRental']
		self.endRental.update(endRental)

	# Delete for Idle
	def deleteIdle(self, key):
		#idle_var = availability['idle']
		self.idle.pop(key, None)

	# Delete for Smart Rental
	def deleteStartRental(self, key):
		#startRental_var = availability['startRental']
		self.startRental.pop(key, None)

	# Delete for End Rental
	def deleteEndRental(self, key):
		#endRental_var = availability['endRental']
		self.endRental.pop(key, None)


	#getidle function
	def getIdle(self, key=''):
		if(key!= ''):
			if key in self.idle:
				return self.idle[key]
			else:
				return 
		else:
			return self.idle;			
	
	"""
	Only works for ditionary with length 2 i.e t and t+1 both
	Takes both data and classifies them into different sub dictionary i.e for idle, startrental, endrental
	At a time an entry will exist in any of the three categories; so before adding to a dictionary, I remove it from other categories
	"""
	def classifyData(self, data):
		if(len(data) ==1):
			vehicles = data['t']
			print 'Can not classify into idle, startrental, endrental as only a single snapshot is available'
			#for car in vehicles:
			#	print car['vin']
				#self.setIdle({car['vin'] : car })

		else:
			#print 't with t+1'
			#print str(data);
			vehicles_prev = data['t']
			vehicles = data['t+1']
			#f = open('workfile', 'w')
			#f.write(str(data))
			#print time.time()

			current_idle = {}
			for car in vehicles_prev:
				current_idle[car['vin']] = car
			vehicles_new = {}
			for car in vehicles:
				vehicles_new[car['vin']] = car
		


			d = DictDiffer(vehicles_new, current_idle )
			idle = d.unchanged()

			for vin in idle:
				#print 'Print for idle'
				self.deleteStartRental(vin)
				self.deleteEndRental(vin)

				#Set Idle Entries
				self.setIdle({ vin: { 'coordinates' : vehicles_new[vin]['coordinates'], 'address' : vehicles_new[vin]['address'] }  })
			end_rental = d.added()

			for vin in end_rental:
				#print 'Print for endrental'
				#print vehicles_new
				self.deleteIdle(vin)
				self.deleteStartRental(vin)

				self.setEndRental({ vin: { 'coordinates' : vehicles_new[vin]['coordinates'], 'address' : vehicles_new[vin]['address'] }})
		
			start_rental = d.removed();

			for vin in start_rental:
				#print 'Print for startrental'
				self.deleteIdle(vin)
				self.deleteEndRental(vin)
				self.setStartRental({ vin: { 'coordinates' : current_idle[vin]['coordinates'], 'address' : current_idle[vin]['address'], 'time': time.strftime('%l:%M%p %Z on %b %d, %Y') }})
