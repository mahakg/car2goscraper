class DictDiffer(object):
#Calculate the difference between two dictionaries

	def __init__(self, current_dict, past_dict):
		self.current_dict, self.past_dict = current_dict, past_dict
		self.set_current, self.set_past = set(current_dict.keys()), set(past_dict.keys())
		self.intersect = self.set_current.intersection(self.set_past)

	# items added
	def added(self):
		return list(self.set_current - self.intersect) 

	# items removed
	def removed(self):
		return list(self.set_past - self.intersect)

	# items removed
	def unchanged(self):

		return list(self.intersect)
