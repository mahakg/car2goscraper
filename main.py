#!/usr/bin/python
import urllib
import json
import urllib2
import time
import codecs
from Car2go import Car2go


#Configuration variables
n= 5 # number of times this code has to be repeated. I kept this to have control how many times you want to run your code.
url = 'https://www.car2go.com/api/v2.1/vehicles'
data_snapshot = {}

params = {}
params['loc'] = 'Berlin';
params['oauth_consumer_key']='addasecrethashhere';
params['format'] = 'json';

sleep_counter = 10

crawl_url = url + '?' + urllib.urlencode(params)
#crawl_url = "http://localhost/a.txt" # for local debugging so I don't need to hit the api server as that can inturn block my IP if lot of hits
#print crawl_url
i=0
d = Car2go(); 
#response = urllib2.urlopen(crawl_url)
f = open('car2go_output.csv', 'w') # File write for local debuggingcodecs.open("test_output", "w", "utf-8-sig")
f.write ("Snapshot at" + " | " + "Status" + " | " + "CarId" + " | " + "Car Co-ordinates" + " | " + " Car location" + " | " + "Timestamp" + "\n")
while True: # Run this loop until maximun n is reached
	print crawl_url
	response = urllib2.urlopen(crawl_url)
	
	if response.getcode() == 200: # continue only if the status code is 200
		data = json.loads(response.read())
		#print data;

		#print str(i) + '---' + str(len(data_snapshot));
	# Maintain snapshots in a dictionary with keys t and t+1, After each new call to API, t will be over ridden by t+1 and t+1 will have new data
		if(len(data_snapshot) == 0):
			data_snapshot['t'] = data['placemarks'];
		elif(len(data_snapshot) == 1):
			data_snapshot['t+1'] = data['placemarks'];
			#d.classifyData(data_snapshot);
		elif(len(data_snapshot) == 2): # the code will start runni
			data_snapshot['t'] = data_snapshot['t+1'];
			#response = urllib2.urlopen("http://localhost/b.txt")
			#data = json.loads(response.read())
			data_snapshot['t+1'] = data['placemarks'];
			
		d.classifyData(data_snapshot);


		#print d;
		print 'Free Cars in ' + params['loc'] +' at ' + time.strftime('%l:%M%p %Z on %b %d, %Y')
		time_str = time.strftime('%l:%M%p %Z on %b %d, %Y');
		#print "Writing to file for Idle at time " + time_str;
		for car_id, car_attr in d.idle.items():
			address = car_attr['address']
			address = address.encode("utf-8")
			f.write (time_str + " | " + "Idle" + " | " + car_id + " | " + str(car_attr['coordinates']) + " | ")
			f.write(address)
			f.write("\n")
		#print "Writing to file for startRental at time " + time_str;
		for car_id, car_attr in d.startRental.items():
			address = car_attr['address']
			address = address.encode("utf-8")
			f.write (time_str + " | " + "StartRental" + " | " + car_id + " | " + str(car_attr['coordinates']) + " | ")
			f.write(address)
			f.write(" | " + car_attr['time'])
			f.write("\n")
		#print "Writing to file for endRental at time " + time_str;
		for car_id, car_attr in d.endRental.items():
			address = car_attr['address']
			address = address.encode("utf-8")
			f.write (time_str + " | " + "EndRental" + " | " + car_id + " | " + str(car_attr['coordinates']) + " | ")
			f.write(address)
			f.write("\n")

		
		#Write to local file for debugging
		#f.write('snapshot of cars at ' + time.strftime('%l:%M%p %Z on %b %d, %Y') + "\n")
		#f.write(str(d));
		#f.write("\n");

		time.sleep(sleep_counter);
	else:
		print "Status code found" + response.getcode() + " Call Failed"

	i = i+1
	if i>=n:
		print 'Process completed... ' + ' All Data is saved in Car2go_output file in the same directory'
		f.close()
		break
